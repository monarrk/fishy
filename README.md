# Fishy!
A discord bot made for Question of the Day in Oceanland!

### Building
Assuming you have [Rust](https://rust-lang.org) installed, a simple `cargo run` should build fishy and its dependancies.

### Usage
Firstly, you need to create a database file. Make an empty file and run `fishy create path/to/database`

After that, the command syntax is `fishy path/to/database TOKEN`, where TOKEN is the token for the bot you're running.
